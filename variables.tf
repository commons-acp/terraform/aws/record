variable "domain_name" {
  description = ""
  type = string
}

variable "record_name" {
  description = ""
  type = string
}
variable "record_type" {
  description = ""
  type = string
}
variable "record_ttl" {
  description = ""
  type = string
}
variable "records" {
  description = ""
  type = list(string)
}
