
resource "aws_route53_record" "CNAME" {
  count   = var.record_type == "CNAME" ? 1 : 0
  zone_id   =  data.aws_route53_zone.selected.zone_id
  name    = var.record_name
  type    = "CNAME"
  ttl     = var.record_ttl
  records = var.records
}

resource "aws_route53_record" "A" {
  count   = var.record_type == "A" ? 1 : 0
  zone_id   =  data.aws_route53_zone.selected.zone_id
  name    = var.record_name
  type    = "A"
  ttl     = var.record_ttl
  records = var.records
}

resource "aws_route53_record" "MX" {
  count   = var.record_type == "MX" ? 1 : 0
  zone_id   =  data.aws_route53_zone.selected.zone_id
  name    = var.record_name
  type    = "MX"
  ttl     = var.record_ttl
  records = var.records
}

resource "aws_route53_record" "TXT" {
  count   = var.record_type == "TXT" ? 1 : 0
  zone_id   =  data.aws_route53_zone.selected.zone_id
  name    = var.record_name
  type    = "TXT"
  ttl     = var.record_ttl
  records = var.records
}

data "aws_route53_zone" "selected" {
  name         = var.domain_name
  private_zone = false
}
